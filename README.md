Data Driven Chemistry Session 6 - Student Version
=================================================

This repository contains the student files required for 
session 6 of the September 2020 Data-driven Chemistry (DDC) course.

Aims of the Session
-------------------

This session builds on the previous topics covered, and will focus on 
how to handle real data that 
you might get from an experiment, from the process of importing
it into Python and correcting any errors through to fitting a model 
to the data and plotting the results. The topics covered include:

- Importing data with non-standard formats
- 'Cleaning' the data
	- Removing/replacing missing or incorrect values
	- Changing data types
	- Manipulating and standardising text strings
- Fitting a model with linear regression
	- Understand what linear regression is and when it is suitable
	- Fitting linear models with Python
	- The essential measures of how good a fit is
	- Extracting uncertainties from linear fits

Files
-----

- The main Jupyter notebook for the session is found in `session6_student.ipynb`
- Images used in the notebooks are stored in the `images` folder
- Example data files (for instance those used in the class tasks) are stored in `data_sources`


James Cumby, james.cumby@ed.ac.uk, September 2020